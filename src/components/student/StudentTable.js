import React, {Component} from 'react';
import {Table, TableHeader, TableRow, TableHeaderColumn, TableBody, TableRowColumn} from 'material-ui';

const styles = {
	columnHeader: {
		color:'black',
		fontSize: '18px'
	},
	columnContent:{
		color:'black',
		fontSize: '16px'
	}
};

export default class StudentTable extends Component {

	render() {

		
		return (
			<Table
				selectable={this.props.selectable}
				onRowSelection={this.props.onRowSelection}
				>
				<TableHeader
					displaySelectAll={false}
					adjustForCheckbox={false}>
					<TableRow>
						<TableHeaderColumn style={styles.columnHeader}>District ID</TableHeaderColumn>
						<TableHeaderColumn style={styles.columnHeader}>Student Name</TableHeaderColumn>
                        <TableHeaderColumn style={styles.columnHeader}>Grade Level</TableHeaderColumn>
                        <TableHeaderColumn style={styles.columnHeader}>Allowance</TableHeaderColumn>
					</TableRow>
				</TableHeader>
				<TableBody 
					displayRowCheckbox={false}
					>
					{this.props.students.map((student, index) => (
						<TableRow className={this.props.selectable ? 'clickable' : ''} key={index}>
							<TableRowColumn style={styles.columnContent}>{student.districtId}</TableRowColumn>
							<TableRowColumn style={styles.columnContent}>{student.firstName} {' '} {student.lastName}</TableRowColumn>
                            <TableRowColumn style={styles.columnContent}>{student.gradeLevelId}</TableRowColumn>
                            <TableRowColumn style={styles.columnContent}> {'$'}{student.allowance}</TableRowColumn>
						</TableRow>
					))}
				</TableBody>
			</Table>
		);
	}

	handleSelect(row, col, event) {
		this.props.onSelect(row);
	}

}

StudentTable.defaultProps = {
	selectable: false
}