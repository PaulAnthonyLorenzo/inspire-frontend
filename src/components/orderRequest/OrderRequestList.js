import React, {Component} from 'react';
import {Table, TableHeader, TableRow, TableHeaderColumn, TableBody, TableRowColumn} from 'material-ui';

const styles = {
	columnHeader: {
		color:'black',
		fontSize: '18px'
	},
	columnContent:{
		color:'black',
		fontSize: '16px'
	}
};

export default class OrderRequestList extends Component {
	render() {
		
		return (
			<Table
				selectable={false}
				>
				<TableHeader
					displaySelectAll={false}
					adjustForCheckbox={false}>
					<TableRow>
						<TableHeaderColumn style={styles.columnHeader}>District ID</TableHeaderColumn>
						<TableHeaderColumn style={styles.columnHeader}>Student Name</TableHeaderColumn>
                        <TableHeaderColumn style={styles.columnHeader}>Requestor Name</TableHeaderColumn>
                        <TableHeaderColumn style={styles.columnHeader}>Order Status</TableHeaderColumn>
						<TableHeaderColumn style={styles.columnHeader}>Vendor</TableHeaderColumn>
						<TableHeaderColumn style={styles.columnHeader}>Total Amount</TableHeaderColumn>
					</TableRow>
				</TableHeader>
				<TableBody 
					displayRowCheckbox={false}
					>
					{this.props.orderRequests.map((orders, index) => (
						<TableRow key={index}>
							<TableRowColumn style={styles.columnContent}>{orders.districtID}</TableRowColumn>
							<TableRowColumn style={styles.columnContent}>{orders.studentName}</TableRowColumn>
                            <TableRowColumn style={styles.columnContent}>{orders.requestorName}</TableRowColumn>
                            <TableRowColumn style={styles.columnContent}>{orders.orderRequestStatus}</TableRowColumn>
							<TableRowColumn style={styles.columnContent}>{orders.vendor}</TableRowColumn>
							<TableRowColumn style={styles.columnContent}>{'$'}{orders.totalAmount}</TableRowColumn>
						</TableRow>
					))}
				</TableBody>
			</Table>
		);
	}

	handleSelect(row, col, event) {
		this.props.onSelect(row);
	}

}