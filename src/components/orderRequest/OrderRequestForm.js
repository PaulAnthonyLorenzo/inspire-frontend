import React, {Component} from 'react';
import {connect} from 'react-redux';
import {reduxForm, Field, values} from 'redux-form';
import { RadioButton } from 'material-ui/RadioButton';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import {TextField, RadioButtonGroup, SelectField} from 'redux-form-material-ui';
import HiddenField from '../common/HiddenField';
import OrderTypes from '../../constants/OrderTypes';
import OrderRequestActions from '../../actions/OrderRequestActions';


const styles = {
    floatingLabelStyle: {
        
    },
    floatingLabelFocusStyle: {
        
    },
    underlineFocusStyle: {
        
    },
    studentIdStyle: {
        display: 'none'
    }
}

const validate = values => {
    const errors = {};
    const {vendorId, vendorName, vendorUrl, otherVendor} = values;
    const vendorNameIsNull = vendorName == null || vendorName.trim() == "";
    const vendorUrlIsNull = vendorUrl == null || vendorUrl.trim() == "";
    const vendorIdIsNull = vendorId == null;

    if(otherVendor) {
        if(vendorNameIsNull) {
            errors.vendorName = "Vendor Name is required";
        }
        if(vendorUrlIsNull) {
            errors.vendorUrl = "Vendor URL is required";
        }
    } else {
        if(vendorIdIsNull) {
            errors.vendorId = "Please select a vendor";
        }
    }
    return errors
  }

class OrderRequestForm extends Component {

    constructor() {
        super();
        this.state = {
            orderType: OrderTypes.Product,
            otherVendor: false,
            vendorId: null,
            vendorName: null,
            vendorUrl: null
        }
    }
    
    componentWillMount() {        
        const {otherVendor, orderType, vendorId, vendorName, vendorUrl} = this.state;
        this.props.change('studentId', this.props.student.studentId);
        this.props.change('orderRequestStatusId', "FOR_APPROVAL");
        this.props.change('requesterEmail', this.props.requester);
        this.props.change('orderType', orderType);
        this.props.change('vendorId', vendorId);
        this.props.change('vendorName', vendorName);
        this.props.change('vendorUrl', vendorUrl);
        this.props.change('otherVendor', otherVendor);
    }

    handleOrderTypeChange(event, value) {
        this.setState({orderType: value});
    }

    handleVendorTextFieldChange(e, field) {
        this.setState({[field]: e.target.value});
    }

    handleVendorChange(event, value) {
        this.setState({vendorId: value});
    }

    handleCheckboxChange() {
        this.setState(prevState => {

            const otherVendor = !this.state.otherVendor;
            let {vendorId, vendorName, vendorUrl} = this.state;
            
            if (otherVendor) {
                vendorId = null;
                this.props.change('vendorId', vendorId);
            } else {
                vendorName = vendorUrl = null;
                this.props.change('vendorName', vendorName);
                this.props.change('vendorUrl', vendorUrl);
            }
            this.props.change('otherVendor', otherVendor);
            
            return {otherVendor, vendorId, vendorName, vendorUrl}
        });        
    }

    submit(values) {
        this.props.save(values);
    }
    
    renderSelectedStudent() {
        
        const {districtId, firstName, lastName} = this.props.student;
        const name = `${firstName} ${lastName}`;
        
        return (
            <div>
                <h3>Student</h3>
                <div>{districtId}</div>
                <div>{name}</div>
            </div>
        );
    }

    renderRequestor() {
        return (
            <div>
                <h3>Requestor</h3>
                <div>{this.props.requestor}</div>
            </div>
        );
    }

    renderVendors() {

        return (
            <div>
                 <h3>Vendor</h3>
                 <Checkbox
                    label="Others"
                    checked={this.state.otherVendor}
                    onCheck={this.handleCheckboxChange.bind(this)}
                    />
                <div className={this.state.otherVendor ? 'hidden' : ''}>
                <Field onChange={this.handleVendorChange.bind(this)} floatingLabelText="Vendor" name="vendorId" value={this.state.vendorId}  component={SelectField}>
                    {this.props.vendors.map(vendor => (
                        <MenuItem key={vendor.id} value={vendor.id} primaryText={vendor.name} />
                    ))}
                </Field>
                </div>
                <div className={this.state.otherVendor ? '' : 'hidden'}>
                    <Field 
                        name='vendorName'
                        component={TextField}
                        floatingLabelText='Vendor Name'
                    />
                    <Field 
                        name='vendorUrl'
                        component={TextField} 
                        floatingLabelText='Vendor URL'
                    />
                </div>
            </div>
        );
    }

    renderOrderTypes() {
        return (
            <div>
                <h3>Order Type</h3>

                <Field name="orderType" onChange={this.handleOrderTypeChange} component={RadioButtonGroup}>
                    <RadioButton
                        value={OrderTypes.Product}
                        label='Product'
                    />
                    <RadioButton
                        value={OrderTypes.Service}
                        label='Service'
                    />
                </Field>
            </div>
        );
    }
    
    
    render() {
        

        return (
            <div>
                {this.renderSelectedStudent()}
                {this.renderRequestor()}
                <form onSubmit={this.props.handleSubmit(this.submit.bind(this))}>
                    {this.renderOrderTypes()}
                    {this.renderVendors()}  
                    <RaisedButton label="Save" primary={true} onClick={this.props.handleSubmit(this.submit.bind(this))} />
                </form>
            </div>
        );
    }
}


OrderRequestForm = reduxForm({form: 'orderRequest', validate})(OrderRequestForm);

export default connect(
    state => ({
        
    }),
    dispatch => ({
        async save(orderRequest) {
            await dispatch(OrderRequestActions.saveOrderRequest(orderRequest));
        }
    })
)(OrderRequestForm);