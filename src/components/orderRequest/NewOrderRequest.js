import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import SearchBar from '../common/SearchBar';
import StudentTable from '../student/StudentTable';
import {DashboardActions, VendorActions} from '../../actions/'
import {connect} from 'react-redux';
import OrderRequestForm from './OrderRequestForm';
import OrderItemForm from '../orderItem/OrderItemForm';
import RaisedButton from 'material-ui/RaisedButton';
import {submit} from 'redux-form';


class NewOrderRequest extends Component {

    constructor() {
        super();
        this.state = {
            keyword:'',
            student: null,
            orderRequest: null,
            orderItems: []
        };
    }

    componentWillMount() {
        this.props.getVendorsByType();
        this.props.getAllStudents();
    }

    handleRowSelection(row) {
        const { studentList } = this.props.studentHolder;
        const student = studentList[row] != null ? studentList[row] : null;
        this.setState({student});       
    }

    handleOrderItemFormSubmit() {
       const values = Object.keys(this.props.forms).map(form => this.props.forms[form].values);
       this.setState({})
       
        
    }

    renderStudentTable() {
        
        return (
            <StudentTable 
                selectable={true}
                students={this.props.studentHolder.studentSearchPage.content}
                onRowSelection={this.handleRowSelection.bind(this)}
            />
        );
    }

    renderPlaceholder() {
        return (
            <EmptyContentPlaceholder text={
                this.props.keyword
                    ? `No results for "${this.props.keyword}"`
                    : 'Use the search bar to look for a student'
            } />
        );
    }

    renderStudentSearch() {
        return (
            <div>
            <h1>Please Select a Student</h1>
             <SearchBar 
                    placeholder='Search student by name'
                    onSubmit={this.props.doSearch.bind(this)} 
                    items={this.props.studentHolder.studentList}
                    />
                <div>   
                    {this.props.studentHolder.studentSearchPage.content && this.props.studentHolder.studentSearchPage.content.length > 0 && this.props.studentHolder.studentSearchPage.content != []
                                ? this.renderStudentTable()
                                : this.renderPlaceholder()
                                }
                </div>
            </div>
        );
    }

    

    renderOrderRequestForm() {
        return (
            <div>
                <OrderRequestForm vendors={this.props.vendors} requester={this.props.activeUser} student={this.state.student} />
            </div>
        );
    }

    renderOrderItemForm() {
        return (
            <div>
                hey
            </div>
        );
    }
    

    render() {

        const renderNoOrderRequest = this.state.student == null ? this.renderStudentSearch() : this.renderOrderRequestForm();
        return (
            <div>
                {this.state.student == null ? this.renderStudentSearch() : this.renderOrderRequestForm()}
                {/* <OrderItemForm form="form-1" onSubmit={this.handleOrderItemFormSubmit.bind(this)} />
                <OrderItemForm form="form-2" onSubmit={this.handleOrderItemFormSubmit.bind(this)} />
                <div style={{width: '100vw', height: 50, borderColor: 'black', borderWidth: 1, borderStyle: 'dashed'}}>
asd
                </div> */}
                
            </div>
        );
    }

}


export default connect(
	(state, ownProps) => ({
        ...ownProps,
        studentHolder: state.dashboard.students,
        activeUser: state.login.activeUser,
        vendors: state.vendor.vendors,
        forms: state.form
	}),
	(dispatch, ownProps) => ({
        ...ownProps,
        saveOrderItem() {
            dispatch(submit('orderItemForm'));
        },
        async doSearch(keyword, page = 0) {
            this.setState({
                keyword: keyword,
                currentPage: page
            })
			await dispatch(DashboardActions.searchStudents(keyword, page))
        },
        
        async nextPageSearch(keyword, page) {
			await dispatch(DashboardActions.searchStudents(keyword, page))
		},
        
        async getAllStudents(){
            await dispatch(DashboardActions.getAllStudents());
        },

        async getVendorsByType(type){
            await dispatch(VendorActions.getVendorsByType(type));
        }
	})
)(NewOrderRequest);