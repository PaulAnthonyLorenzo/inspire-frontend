import OrderRequestList from './OrderRequestList';
import OrderRequestActions from '../../actions/OrderRequestActions';
import RaisedButton from 'material-ui/RaisedButton';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import StatusBox from '../common/statusBox/StatusBox';
import StatusBoxContainer from '../common/statusBox/StatusBoxContainer';
import AppIcons from '../../constants/AppIcons';
import AppSvgIcon from '../common/AppSvgIcon';
import {colors} from 'material-ui/styles';
import StatusBoxes from '../../constants/StatusBoxes';
import { Link } from 'react-router-dom';
import Routes from '../../constants/Routes';

const styles = {
    createRequestStyle: {
        backgroundColor: colors.green100,
        color: 'black'
    },
    actionButtonContainerStyle: {

    }
};

class OrderRequest extends Component {
    
    constructor() {
        super();
        this.state = {
			keyword: ''
		};
    }

    componentDidMount() {
        this.props.getAllRequestOrders();	
        this.props.getCount();
    }
    render() {
        const stats = StatusBoxes.map(s => {
            const a = this.props.count.filter(c => c.statusId == s.id);
            const cc = a.length > 0 ? a[0].count : 0;
           
            return {...s, count: cc}
        });
        
        return (
            <div><StatusBoxContainer statusBoxes={stats}/>
            
            <div style={styles.actionButtonContainerStyle} className="row end-xs">
                <div className="col-lg">
                    <Link to={Routes.orderRequest.new}>
                    <RaisedButton
                        className="box"
                        label="Create Order Request"
                        backgroundColor={colors.green100}
                        icon={<AppSvgIcon icon={AppIcons.add} />}
                        labelColor={colors.black}
                    />
                    </Link>
                </div>
            </div>
            <OrderRequestList orderRequests={this.props.orderRequests} />
            </div>
        )
    }
}

export default connect(
	(state, ownProps) => ({
        ...ownProps,
        orderRequests: state.orderRequest.orderRequest,
        activeUser: state.login.activeUser,
        count: state.orderRequest.count
	}),
	(dispatch, ownProps) => ({
        ...ownProps,
        
        async doSearch(keyword, page = 0) {
			await dispatch(DashboardActions.searchStudents(keyword, page))
		},
        
        getAllRequestOrders(){
            dispatch(OrderRequestActions.getAllRequestOrders());
        },

        getCount(){
            dispatch(OrderRequestActions.getCount());
        }
	})
)(OrderRequest);