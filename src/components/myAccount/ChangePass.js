import React, { Component } from 'react';
import { connect } from 'react-redux';
import { submit } from 'redux-form';
import TextField from 'material-ui/TextField';
import SvgIcon from 'material-ui/SvgIcon';
import RaisedButton from 'material-ui/RaisedButton';
import { Redirect } from 'react-router';
import { bindActionCreators } from 'redux';
import { Link, Route, BrowserRouter as Router, browserHistory } from 'react-router-dom';
import Routes from '../../constants/Routes';

import ChangePassForm from './ChangePassForm';
import { MyAccountService } from '../../services/MyAccountService';
import { MyAccountActions } from '../../actions/MyAccountActions';


const styles = {
    buttonStyle: {
        paddingTop: 20,
        display:'flex',
        justifyContent: 'space-around',
      },
}

class ChangePass extends Component {

  render() {

    return (
      <div>
          <div>
          <ChangePassForm onSubmit={password => this.validatePassword(password)}/>
          </div>
          <div style={styles.buttonStyle}>
            <RaisedButton label="Submit" onClick={() => this.props.submit()}/> 
          </div>
          <div>
            {this.props.error && this.props.error.map((message, i) => 
                <div style={styles.failedText} key={i}>{message}</div>)}
            {this.props.success && this.props.success.map((message, i) => 
                <div style={styles.successText} key={i}>{message}</div>)}
          </div>
      </div>
    )
  }

  validatePassword(password) {
    this.props.validatePassword(password);
  }
  
}

export default connect(
  (state, ownProps) => ({
    ...state,
    ...ownProps,
    error: state.myAccountReducer.error,
    success: state.myAccountReducer.success
  }),
  (dispatch, ownProps) => ({
    ...ownProps,
    
    async validatePassword(password) {
      console.log(password)
      await dispatch(MyAccountActions.validatePassword(password));
    },
    submit() {
      dispatch(submit('changePassForm'));
    },
  })
)(ChangePass);