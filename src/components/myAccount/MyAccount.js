import React, { Component } from 'react';
import { connect } from 'react-redux';
import { submit } from 'redux-form';
import TextField from 'material-ui/TextField';
import SvgIcon from 'material-ui/SvgIcon';
import RaisedButton from 'material-ui/RaisedButton';
import { Redirect } from 'react-router';
import { bindActionCreators } from 'redux';
import { Link, Route, BrowserRouter as Router, browserHistory } from 'react-router-dom';
import Routes from '../../constants/Routes';

class MyAccount extends Component {

  render() {

    return  (
        <div>
            <Link to={Routes.myAccount.changePassword}>
                <RaisedButton label="Change Password"/>
            </Link>
        </div>
    )
  }

}

export default connect(
  (state, ownProps) => ({
    ...state,
    ...ownProps,
  }),
  (dispatch, ownProps) => ({
    ...ownProps,
  })
)(MyAccount);