import React, {Component} from 'react';
import {connect} from 'react-redux';
import {reduxForm, Field} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import RaisedButton from 'material-ui/RaisedButton';
import SvgIcon from 'material-ui/SvgIcon';
import AppIcons from '../../constants/AppIcons';
import AppSvgIcon from '../common/AppSvgIcon';
import { withRouter } from 'react-router-dom';
import Loader from '../common/Loader';

const styles = {
  floatingLabelFocusStyle: {
    color: "#000000"
  },
  floatingLabelStyle: {
    fontSize: 16,
    color: "#000000"
  },
  inputStyle: {
    fontSize: 14,
    WebkitBoxShadow: '0 0 0 1000px #FFFFFF inset',
  },
  underlineFocusStyle: {
    borderColor: "#000000"
  },
  inputIconStyle: {
    width: 30,
    height: 30,
    marginRight: 15,
    marginTop: 35
  },
  fieldErrorStyle: {
    textAlign: 'left'
  },
  tipText: {
    fontFamily: "'Roboto', sans-serif",
    fontWeight: 400,
    fontSize: 14,
    fontStyle: 'italic',
    color: '#00000',
    textAlign: 'center',
  },
  
}

const validateChangePassForm = values => {
    const errors = {};
    
    let passwordPattern = /^[a-zA-Z0-9]{8,16}$/;
    
    // Format
    if (values.currentPassword == null) {
      errors.currentPassword = 'This field is required';
    }
    if (!passwordPattern.test(values.currentPassword)) {
      errors.currentPassword = 'This is not a valid password';
    }
    if (values.newPassword == null) {
      errors.newPassword = 'This field is required';
    }
    if (!passwordPattern.test(values.newPassword)) {
      errors.newPassword = 'This is not a valid password';
    }
    if (values.confirmPassword == null) {
      errors.confirmPassword = 'This field is required';
      }
    if (!passwordPattern.test(values.confirmPassword)) {
      errors.confirmPassword = 'This is not a valid password';
    }

    // Compare
    if (values.newPassword != values.confirmPassword && values.confirmPassword != null) {
      errors.confirmPassword = 'Passwords don\'t match!';
    }
    
    return errors;
  }
  


class ChangePassForm extends Component {

  constructor(props){
    super(props);
    this.state = {
      type: 'password'
    }
    this.showHide = this.showHide.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.handleSubmit()
  } 

  showHide(event){
    event.preventDefault();
    event.stopPropagation();
    this.setState({
      type: this.state.type === 'input' ? 'password' : 'input'
    })  
  }

  render() {
    return (
      <form>
      <div className="row center-xs inline">
      <Field
      name='currentPassword'
      type='password'
      component={TextField}
      errorStyle={styles.fieldErrorStyle}
      floatingLabelText='Current Password' 
      floatingLabelStyle={styles.floatingLabelStyle}
      floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
      inputStyle={styles.inputStyle}
      underlineFocusStyle={styles.underlineFocusStyle}
      />
      </div>
      <div style={styles.tipText}>
          <div style={{marginTop: '3%'}}>Password must be at least 8 characters long and a maximum of 16 characters.</div>
          <div>Can only be consist of lowercase or uppercase characters and numbers.</div>
      </div>
      <div className="row center-xs">
        <div>
          <Field
          name='newPassword'
          type={this.state.type}
          component={TextField}
          errorStyle={styles.fieldErrorStyle}
          floatingLabelText='New Password' 
          floatingLabelStyle={styles.floatingLabelStyle}
          floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
          inputStyle={styles.inputStyle}
          underlineFocusStyle={styles.underlineFocusStyle}
          />
        </div>
          <div>
            <span onClick={this.showHide}>
              {this.state.type === 'input' 
                      ? <AppSvgIcon
                      viewBox= '0 0 50 50'
                      icon={AppIcons.hide}
                      style={styles.inputIconStyle}
                      color="#616161"
                      /> 
                      : <AppSvgIcon
                      viewBox= '0 0 50 50'
                      icon={AppIcons.show}
                      style={styles.inputIconStyle}
                      color="#616161"
                      />}
            </span>
          </div>
      </div>
      
      <div className="row center-xs">
        <div>
          <Field
          name='confirmPassword'
          type={this.state.type}
          component={TextField}
          errorStyle={styles.fieldErrorStyle}
          floatingLabelText='Confirm Password' 
          floatingLabelStyle={styles.floatingLabelStyle}
          floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
          inputStyle={styles.inputStyle}
          underlineFocusStyle={styles.underlineFocusStyle}
          />
        </div>
        <div>
              <span onClick={this.showHide}>
                {this.state.type === 'input' 
                        ? <AppSvgIcon
                        viewBox= '0 0 50 50'
                        icon={AppIcons.hide}
                        style={styles.inputIconStyle}
                        color="#616161"
                        /> 
                        : <AppSvgIcon
                        viewBox= '0 0 50 50'
                        icon={AppIcons.show}
                        style={styles.inputIconStyle}
                        color="#616161"
                        />}
              </span>
            </div>
        </div>
      </form>
    )
  }
}

ChangePassForm = reduxForm({form: 'changePassForm'})(ChangePassForm);

export default connect(
  state => ({
    validate: validateChangePassForm
  })
)(ChangePassForm);