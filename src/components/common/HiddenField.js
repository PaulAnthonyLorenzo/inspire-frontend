import React from 'react';


const HiddenField = ({value, className}) => <input type='hidden' value={value} className={className}  /> 

export default HiddenField; 