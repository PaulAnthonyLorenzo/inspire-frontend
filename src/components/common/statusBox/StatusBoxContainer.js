import React, {Component} from 'react';
import {connect} from 'react-redux';
import StatusBox from './StatusBox';
import OrderRequestActions from '../../../actions/OrderRequestActions';


class StatusBoxContainer extends Component {


    constructor() {
        super(); 
    }

    render() {

        
        return  (<div className="row">
        {this.props.statusBoxes.map((statusBox, index) => {
            const {title, count, icon, color, id} = statusBox;
            return (
            <StatusBox onClick={() => this.props.onBoxClick(title)} key={index} title={title} color={color} count={count} icon={icon} id={id} classes="col-xs-12 col-sm-3 col-md-3 col-lg"/>
            );
        })}
        </div>);
    }
}

export default connect(
	(state, ownProps) => ({
        ...ownProps
	}),
	(dispatch, ownProps) => ({
        ...ownProps,
        onBoxClick(title){
            console.log(title);
            dispatch(OrderRequestActions.getFilteredRequest(title));
        }
	})
)(StatusBoxContainer);