import React, {Component} from 'react';
import {Card, CardText} from 'material-ui/Card';
import AppSvgIcon from '../AppSvgIcon';

const styles = {
	containerStyle: {	
		height: "14vh",
		paddingRight: 5,
		marginBottom: 10
	},
	statusBoxStyle: {
	},
	cardTextStyle: {
		color: 'white'
	},
	countStyle: {
		fontSize: 35
	},
	iconStyle: {
		width: 45,
		height: 45,
	},
	iconContainerStyle: {
		textAlign: 'right',
		paddingBottom: -20
	},
	titleStyle: {
		fontSize: 14
	}
};

const StatusBox = ({title, count, icon, iconStyle, classes, color, style, onClick}) => 
					<div onClick={onClick} style={{style, ...styles.statusBoxStyle}} className={classes}>
						<Card style={{backgroundColor: color, ...styles.containerStyle}} className="box clickable">
							<CardText  style={styles.cardTextStyle}>
								<div className="row between-xs">
									<div className="col-lg-6">
										<div style={styles.countStyle} className="box">
											{count}
										</div>
									</div>
									<div style={styles.iconContainerStyle} className="col-lg-6">
										<div className="box">
											<AppSvgIcon style={styles.iconStyle} color="white" icon={icon} />
										</div>
									</div>
									<div className="col-lg-12">
										<div style={styles.titleStyle} className="box">
											{title}
										</div>
									</div>
								</div>
							</CardText>
						</Card>
					</div>

export default StatusBox;
