import React, {Component} from 'react';
import {Paper} from 'material-ui';


const styles = {
	paper: {
		margin: '24px auto 24px auto',
		maxWidth: 1024
	}
};

export class ContentContainer extends Component {

	render() {
		return (
			<Paper style={styles.paper}>
				{this.props.children}
			</Paper>
		);
	}

}