import React, {Component} from 'react';
import {colors} from 'material-ui/styles';


const style = {
	margin: 0,
	padding: 24,
	color: colors.grey400,
	textAlign: 'center',
	fontWeight: 'lighter',
	fontSize: 24
};

export default class EmptyContentPlaceholder extends Component {

	render() {
		return (
			<h1 style={style}>{this.props.text || 'No items'}</h1>
		);
	}

}