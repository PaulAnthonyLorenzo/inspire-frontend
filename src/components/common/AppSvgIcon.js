import React, {Component} from 'react';
import SvgIcon from 'material-ui/SvgIcon';


export const generatePath = icon => {
    if(Array.isArray(icon)) 
    return icon.map((d, i) => <path key={i} d={d} />);
    
    
    return <path d={icon} />
    
}

import {defaultViewBox} from '../../constants/AppIcons';


const AppSvgIcon = ({icon, style, color, viewBox}) => {
    
    const vb = viewBox != null && viewBox != undefined ? viewBox : defaultViewBox;
        
        return (
            <SvgIcon
            style={style}
            color={color}
            viewBox={vb}
            >
            {generatePath(icon)}
            </SvgIcon>
        )
    }
    
    
    export default AppSvgIcon;