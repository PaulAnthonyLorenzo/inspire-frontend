import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

const Loader = ({size, thickness}) => (
  <div>
    <CircularProgress size={size} thickness={thickness} />
  </div>
);

export default Loader;