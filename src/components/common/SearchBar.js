import React, {Component} from 'react';
import {Paper, IconButton} from 'material-ui';
import {ActionSearch, NavigationClose} from 'material-ui/svg-icons';
import DatePicker from 'material-ui/DatePicker';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import Autosuggest from 'react-autosuggest';
import AutosuggestTheme from '../../styles/autosuggestTheme.css';


const styles = {
	'searchBar': {
		margin: 12
	},
	'searchBar.searchForm': {
		display: 'flex',
		flexDirection: 'row'
	},
	'searchBar.icon': {
		padding: 12
	},
	'searchBar.searchField': {
		border: 'none',
		display:'flex',
		flex: 1,
		marginRight: 12,
		fontSize: '1em'
	},
	'searchBar.container': {
		marginBottom: 15
	}
}

export default class SearchBar extends Component {

	constructor(props, context) {
		super(props, context);
		this.state = {
			query: props.initialQueryText || '',
			suggestions: []
		};
	}

	getSuggestions(query){
		if(query == undefined) {
			<div>Fetching</div>
		} else {
			let inputValue = query.trim().replace(/  +/g, ' ').toLowerCase();
			let inputLength = inputValue.length;
			const inputArr = inputValue.split(' ');
			const wordCount = inputArr.length;

			if (wordCount > 1) {
				return this.props.list.filter(item => {
					const {firstName, lastName} = item;
					const name = `${firstName}${lastName}`.toLowerCase();
					const nameRev = `${lastName}${firstName}`.toLowerCase();
					const noSpaces = inputValue.replace(' ', '');
					return name.indexOf(noSpaces) !== -1 || nameRev.indexOf(noSpaces) !== -1;
				});
			} else {
				return inputLength === 0 ? [] : this.props.items.filter(item => {
					const {firstName, lastName} = item;

					return inputArr.some(word => word.indexOf(firstName.toLowerCase().slice(0, inputLength)) != -1 || word.indexOf(lastName.toLowerCase().slice(0,inputLength)) != -1)
				})
			}
		}
	};
	  
	  // When suggestion is clicked, Autosuggest needs to populate the input
	  // based on the clicked suggestion. Teach Autosuggest how to calculate the
	  // input value for every given suggestion.
	getSuggestionValue(suggestion) {
		let fullName = suggestion.firstName + " " + suggestion.lastName;
		return (fullName)
	};
	  
	  // Use your imagination to render suggestions.
	renderSuggestion (suggestion) {
		return(
		<div>
		  {suggestion.firstName}{' '}{suggestion.lastName}
		</div>
		)
	};

	onChange(event, {newValue}) {
		this.setState({
		  query: newValue
		});
	};
		
	shouldRenderSuggestions(value) {
		return value.length > 0;
	}
	
	  // Autosuggest will call this function every time you need to update suggestions.
	  onSuggestionsFetchRequested({value}) {
			this.setState({
				suggestions: this.getSuggestions(value)
			});
	  };
	
	  // Autosuggest will call this function every time you need to clear suggestions.
	  onSuggestionsClearRequested() {
			this.setState({
				suggestions: []
			});
	  };

	render() {
		const { query, suggestions } = this.state;
		
			// Autosuggest will pass through all these props to the input.
			const inputProps = {
			  placeholder: this.props.placeholder,
			  value: this.state.query,
			  onChange: this.onChange.bind(this)
			};
		return (
			<div>
			<div style={styles['searchBar.container']}>
				<Paper style={styles['searchBar']}>
					<form style={styles['searchBar.searchForm']} onSubmit={event => this.handleSubmit(event)}>
						<ActionSearch style={styles['searchBar.icon']} />
						<div id="autoSuggest" style={styles['searchBar.searchField']}>
						<Autosuggest
							theme={AutosuggestTheme}
							suggestions={suggestions.splice(0,10)}
							onSuggestionsFetchRequested={this.onSuggestionsFetchRequested.bind(this)}
							onSuggestionsClearRequested={this.onSuggestionsClearRequested.bind(this)}
							getSuggestionValue={this.getSuggestionValue.bind(this)}
							renderSuggestion={this.renderSuggestion}
							inputProps={inputProps}
							shouldRenderSuggestions={this.shouldRenderSuggestions}
						/>
						</div>
						{!!this.state.query ? (
							<IconButton onClick={() => this.clearQuery()}>
								<NavigationClose />
							</IconButton>
						) : null}
						</form>
				</Paper>
			</div>
			
			

			</div>
		);
	}

	clearQuery() {
		this.setState({
			query: ''
		});
	}

	handleChange(event) {
		this.setState({
			query: event.currentTarget.value
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		this.props.onSubmit(this.state.query);
	}
}