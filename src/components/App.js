import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Provider} from 'react-redux';
import {reducer as formReducer} from 'redux-form';
import { connect } from 'react-redux';
import reduxStore, {history} from '../stores/index';
import { BrowserRouter as Router, Route, Redirect, browserHistory, Switch } from 'react-router-dom';
import Login from './login/Login';
import ForgotPass from './login/ForgotPass';
import Home from './home/Home';
import Main from './main/Main';
import 'flexboxgrid/css/flexboxgrid.min.css';
import theme from '../styles/theme';
import Routes from '../constants/Routes';
import { ConnectedRouter } from 'react-router-redux';




class AppComponent extends React.Component { 
  
  render() {
    
    return (
      <MuiThemeProvider muiTheme={theme}>
      <Provider store={reduxStore()}>
        <ConnectedRouter history={history}>
          <div>
            <Switch>
            <Route path={Routes.login} exact component={Login} />
            <Route path={Routes.forgotPassword} exact component={ForgotPass} />
            <Main />
            </Switch>
          </div>
        </ConnectedRouter>
      </Provider>
      </MuiThemeProvider>
    );
  }
}

AppComponent.defaultProps = {
};

export default connect(
  (state, ownProps) => ({
    ...state,
    ...ownProps,
    isLoggedIn: state.login.isLoggedIn
  }),
  (dispatch, ownProps) => ({
    ...ownProps
  })
)(AppComponent);
