import React, {Component} from 'react';
import {connect} from 'react-redux';
import {reduxForm, Field} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import RaisedButton from 'material-ui/RaisedButton';
import SvgIcon from 'material-ui/SvgIcon';
import AppIcons from '../../constants/AppIcons';
import AppSvgIcon from '../common/AppSvgIcon';
import { withRouter } from 'react-router-dom';

const styles = {
  loginInputContainer: {
    marginTop: '15%'
  },
  floatingLabelFocusStyle: {
    color: "#FFFFFF"
  },
  floatingLabelStyle: {
    fontSize: 16,
    color: "#FFFFFF"
  },
  inputStyle: {
    fontSize: 16,
  },
  underlineFocusStyle: {
    borderColor: "#FFFFFF"
  },
  inputIconStyle: {
    width: 30,
    height: 30,
    marginRight: 15,
    marginTop: 35
  },
  fieldErrorStyle: {
    textAlign: 'left'
  }
}

const validateLoginForm = values => {
  const errors = {};
  
  let emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
  if (values.email == null) {
    errors.email = 'This field is required';
  }
  else if (!emailPattern.test(values.email)) {
    errors.email = 'This is not a valid email';
  }
  
  return errors;
}


class LoginForm extends Component {

  handleSubmit(event) {
    event.preventDefault();
    this.props.handleSubmit()
  } 
  render() {
    return (
      <form>
      <div className="row center-xs" style={styles.loginInputContainer}>
      <AppSvgIcon
      icon={AppIcons.user}
      style={styles.inputIconStyle}
      color="#FFFFFF"
      />
      <Field
      name='email'
      component={TextField}
      errorStyle={styles.fieldErrorStyle}
      floatingLabelText='Email' 
      floatingLabelStyle={styles.floatingLabelStyle}
      floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
      inputStyle={styles.inputStyle}
      underlineFocusStyle={styles.underlineFocusStyle}
      />
      </div>
      <div className="row center-xs">
      <AppSvgIcon
      icon={AppIcons.password}
      style={styles.inputIconStyle}
      color="#FFFFFF"
      />
      <Field
      name='password'
      type='password'
      component={TextField}
      errorStyle={styles.fieldErrorStyle}
      floatingLabelText='Password'
      floatingLabelStyle={styles.floatingLabelStyle}
      floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
      inputStyle={styles.inputStyle}
      underlineFocusStyle={styles.underlineFocusStyle}
      />
      </div>
      </form>
    )
  }
}

LoginForm = reduxForm({form: 'loginForm'})(LoginForm);

export default connect(
  state => ({
    validate: validateLoginForm
  })
)(LoginForm);