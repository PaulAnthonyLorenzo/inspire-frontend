import React, {Component} from 'react';
import {connect} from 'react-redux';
import {reduxForm, Field} from 'redux-form';
import {TextField} from 'redux-form-material-ui';
import RaisedButton from 'material-ui/RaisedButton';
import SvgIcon from 'material-ui/SvgIcon';
import AppIcons from '../../constants/AppIcons';
import AppSvgIcon from '../common/AppSvgIcon';
import { withRouter } from 'react-router-dom';
import Loader from '../common/Loader';

const styles = {
  loginInputContainer: {
    marginTop: '7%'
  },
  floatingLabelFocusStyle: {
    color: "#FFFFFF"
  },
  floatingLabelStyle: {
    fontSize: 16,
    color: "#FFFFFF"
  },
  inputStyle: {
    fontSize: 14,
    WebkitBoxShadow: '0 0 0 1000px #04195C inset',
  },
  underlineFocusStyle: {
    borderColor: "#FFFFFF"
  },
  inputIconStyle: {
    width: 30,
    height: 30,
    marginRight: 15,
    marginTop: 35
  },
  fieldErrorStyle: {
    textAlign: 'left'
  },

  
}

const validateForgotPassForm = values => {
  const errors = {};
  
  let emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
  if (values.email == null) {
    errors.email = 'This field is required';
  }
  else if (!emailPattern.test(values.email)) {
    errors.email = 'This is not a valid email';
  }
  
  return errors;
}


class ForgotPassForm extends Component {

  handleSubmit(event) {
    event.preventDefault();
    this.props.handleSubmit()
  } 
  render() {
    return (
      <form>
      <div className="row center-xs" style={styles.loginInputContainer}>
      <AppSvgIcon
      icon={AppIcons.user}
      style={styles.inputIconStyle}
      color="#FFFFFF"
      />
      <Field
      name='email'
      component={TextField}
      errorStyle={styles.fieldErrorStyle}
      floatingLabelText='Email' 
      floatingLabelStyle={styles.floatingLabelStyle}
      floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
      inputStyle={styles.inputStyle}
      underlineFocusStyle={styles.underlineFocusStyle}
      />
      </div>
      </form>
    )
  }
}

ForgotPassForm = reduxForm({form: 'forgotPassForm'})(ForgotPassForm);

export default connect(
  state => ({
    validate: validateForgotPassForm
  })
)(ForgotPassForm);