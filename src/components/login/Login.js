import React, { Component } from 'react';
import { connect } from 'react-redux';
import { submit } from 'redux-form';
import TextField from 'material-ui/TextField';
import SvgIcon from 'material-ui/SvgIcon';
import RaisedButton from 'material-ui/RaisedButton';
import { Redirect } from 'react-router';
import { bindActionCreators } from 'redux';
import { Link, Route, BrowserRouter as Router, browserHistory } from 'react-router-dom';
import Routes from '../../constants/Routes';

import LoginForm from './LoginForm';
import {LoginService} from '../../services/LoginService';
import {LoginActions} from '../../actions/LoginActions';

const loginImage = require('../../images/loginBG.jpg');

const styles = {
  loginMainDiv: {
    overflow: 'hidden',
    width: '100vw',
    height: '100vh',
    backgroundImage: 'url('+ loginImage +')',
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
    backgroundRepeat: 'no-repeat',
    display: 'inline-block',
  },
  loginCard: {
    opacity: .90,
    margin: 'auto',
    width: '40%',
    height: '100%',
    backgroundColor: '#04195C',
    display: 'inline-block',
    position: 'absolute',
    top: 0,
    bottom: 0
  },
  sideCard: {
    opacity: .50,
    width: '30%',
    height: '100%',
    backgroundColor: '#FFFFFF',
    display: 'inline-block'
  },
  loginTitle: {
    fontFamily: "'Roboto', sans-serif",
    fontWeight: 100,
    fontSize: 36,
    color: '#FFFFFF',
    textAlign: 'center',
    marginTop: '30%'
  },
  loginInputContainer: {
    marginTop: '15%'
  },
  floatingLabelFocusStyle: {
    color: "#FFFFFF"
  },
  floatingLabelStyle: {
    fontSize: 16
  },
  inputStyle: {
    fontSize: 16
  },
  underlineFocusStyle: {
    borderColor: "#FFFFFF"
  },
  inputIconStyle: {
    width: 30,
    height: 30,
    marginRight: 15,
    marginTop: 35
  },
  buttonStyle:{
    paddingTop: 20,
    display:'flex',
    justifyContent: 'space-around'
  },
  forgotText:{
    fontFamily: "'Roboto', sans-serif",
    fontWeight: 400,
    fontSize: 14,
    color: '#FFFFFF',
    textAlign: 'center',
    marginTop: '5%'
  }
}

class Login extends Component {

  componentWillMount() {    
    this.props.checkLocalStorage();
  }
  
  render() {


    const {activeUser} = this.props;
    const isLoggedIn = activeUser != null && activeUser != "";

    return isLoggedIn ? <Redirect to={Routes.home}/> :   
     (
      <div style={styles.loginMainDiv}>
        <div style={styles.sideCard}>
        </div>
        <div style={styles.loginCard}>
          <div style={styles.loginTitle}>
            INSPIRE
          </div>
          <LoginForm onSubmit={credentials => this.login(credentials)}/>
          <div>
          </div>
          <div style={styles.buttonStyle}>
            <RaisedButton label="Login" onClick={() => this.props.submit()}/> 
          </div>
          <div style={styles.forgotText}>
              <Link to={Routes.forgotPassword}>Forgot password?</Link>
          </div>
          <div style={{display:'flex', justifyContent:'center', fontSize:'20px', paddingTop: '20px', color:'red'}}>
              {this.renderError(this.props.error)}
          </div>
        </div>
        <div style={{...styles.sideCard, position: 'absolute', right: '0'}}>
        </div>
      </div>

    )
  }

  login(credentials) {
    this.props.login(credentials);
  }

  logout() {
    this.props.logout();
  }
  
  renderError(error){
    if(error == undefined){
      return null
    } else {
      let errorCode = error.errors._error.response.data.status;
      if(errorCode === 401 ){
        return <div>{"Invalid username/password"}</div>
      } else if (errorCode === 500) {
        return <div>{"Server down. Please try again later"}</div>
      }
    }
  }
}

export default connect(
  (state, ownProps) => ({
    ...state,
    ...ownProps,
    activeUser: state.login.activeUser,
    error: state.login.error
  }),
  (dispatch, ownProps) => ({
    ...ownProps,
    login(credentials) {
      dispatch(LoginActions.login(credentials));
    },
    submit() {
      dispatch(submit('loginForm'));
    },
    checkLocalStorage() {
      dispatch(LoginActions.checkLocalStorage());
    }
  })
)(Login);