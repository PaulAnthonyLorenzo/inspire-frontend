import React from 'react';

const loginImage = require('../../images/loginBG.jpg');

const LoginImage = () => (<img src={loginImage} alt="Login Image" />);
LoginImage.displayName = 'LoginImage';

export default LoginImage;
