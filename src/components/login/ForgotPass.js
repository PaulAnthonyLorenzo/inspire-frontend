import React, { Component } from 'react';
import { connect } from 'react-redux';
import { submit } from 'redux-form';
import TextField from 'material-ui/TextField';
import SvgIcon from 'material-ui/SvgIcon';
import RaisedButton from 'material-ui/RaisedButton';
import { Redirect } from 'react-router';
import { bindActionCreators } from 'redux';
import { Link, Route, BrowserRouter as Router, browserHistory } from 'react-router-dom';

import ForgotPassForm from './ForgotPassForm';
import {ForgotPassService} from '../../services/ForgotPassService';
import {ForgotPassActions} from '../../actions/ForgotPassActions';
import Loader from '../common/Loader';

const loginImage = require('../../images/loginBG.jpg');

const styles = {
  loginMainDiv: {
    overflow: 'hidden',
    width: '100vw',
    height: '100vh',
    backgroundImage: 'url('+ loginImage +')',
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
    backgroundRepeat: 'no-repeat',
    display: 'inline-block',
  },
  loginCard: {
    opacity: .90,
    margin: 'auto',
    width: '40%',
    height: '100%',
    backgroundColor: '#04195C',
    display: 'inline-block',
    position: 'absolute',
    top: 0,
    bottom: 0
  },
  sideCard: {
    opacity: .50,
    width: '30%',
    height: '100%',
    backgroundColor: '#FFFFFF',
    display: 'inline-block'
  },
  loginTitle: {
    fontFamily: "'Roboto', sans-serif",
    fontWeight: 100,
    fontSize: 36,
    color: '#FFFFFF',
    textAlign: 'center',
    marginTop: '30%'
  },
  loginInputContainer: {
    marginTop: '15%'
  },
  floatingLabelFocusStyle: {
    color: "#FFFFFF"
  },
  floatingLabelStyle: {
    fontSize: 16
  },
  inputStyle: {
    fontSize: 16
  },
  underlineFocusStyle: {
    borderColor: "#FFFFFF"
  },
  inputIconStyle: {
    width: 30,
    height: 30,
    marginRight: 15,
    marginTop: 35
  },
  buttonStyle: {
    paddingTop: 20,
    display:'flex',
    justifyContent: 'space-around',
    
  },
  forgetInfo: {
    fontFamily: "'Roboto', sans-serif",
    fontWeight: 300,
    fontSize: 14,
    color: '#FFFFFF',
    textAlign: 'center',
    marginTop: '5%'
  },
  successText: {
    fontFamily: "'Roboto', sans-serif",
    fontWeight: 800,
    fontSize: 14,
    color: 'green',
    textAlign: 'center',
    marginTop: '4%'
  },
  failedText: {
    fontFamily: "'Roboto', sans-serif",
    fontWeight: 800,
    fontSize: 14,
    color: 'red',
    textAlign: 'center',
    marginTop: '4%'
  }
}

class ForgotPass extends Component {
  
  render() {
    
    return (
      <div style={styles.loginMainDiv}>
        <div style={styles.sideCard}>
        </div>
        <div style={styles.loginCard}>
          <div style={styles.loginTitle}>
            INSPIRE
          </div>
          <div style={styles.forgetInfo}>
              Please enter your email to reset your password
          </div>
          
          <ForgotPassForm onSubmit={email => this.forgotPass(email)}/>
          <div style={styles.buttonStyle}>
            <RaisedButton label="Reset Password" onClick={() => this.props.submit()}/>
          </div>
          <div style={styles.buttonStyle}>
            <RaisedButton label="Back" href="/login"/>
          </div>
          <div>
            {this.props.error && this.props.error.map((message, i) => 
                <div style={styles.failedText} key={i}>{message}</div>)}
            {this.props.success && this.props.success.map((message, i) => 
                <div style={styles.successText} key={i}>{message}</div>)}
          </div>
          
        </div>
        <div style={{...styles.sideCard, position: 'absolute', right: '0'}}>
        </div>
      </div>

    )
  }

  forgotPass(email) {
    this.props.forgotPass(email);
  }
  
}

export default connect(
  (state, ownProps) => ({
    ...state,
    ...ownProps,
    error: state.forgotPassReducer.error,
    success: state.forgotPassReducer.success
  }),
  (dispatch, ownProps) => ({
    ...ownProps,

    async forgotPass(email) { 
      await dispatch(ForgotPassActions.forgotPass(email));
    },
    submit() {
      dispatch(submit('forgotPassForm'));
    },
  })
)(ForgotPass);