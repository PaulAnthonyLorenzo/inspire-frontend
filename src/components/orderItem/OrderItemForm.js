import React, {Component} from 'react';
import {connect} from 'react-redux';
import {reduxForm, Field} from 'redux-form';
import RaisedButton from 'material-ui/RaisedButton';
import {TextField, RadioButtonGroup, Checkbox} from 'redux-form-material-ui';
import { RadioButton } from 'material-ui/RadioButton';


class OrderItemForm extends Component {

    constructor() {
        super();
        
        
    }

    componentWillMount() {
        this.props.change('commodityType', 'CONSUMABLE');
    }


    render() {
        const {handleSubmit, onSubmit} = this.props;
        return (
            <div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Field 
                            name='description'
                            component={TextField}
                            floatingLabelText='Description'
                        />
                    <Field 
                            name='itemUrl'
                            component={TextField}
                            floatingLabelText='Item Url'
                        />
                    <Field 
                            name='unitPrice'
                            component={TextField}
                            floatingLabelText='Unit Price'
                        />
                    <Field 
                            name='quantity'
                            component={TextField}
                            floatingLabelText='Quantity'
                        />
                    <Field 
                            name='notes'
                            component={TextField}
                            multiLine
                            floatingLabelText='Notes'
                        />
                    <Field name="commodityType" onChange={this.handleOrderTypeChange} component={RadioButtonGroup}>
                        <RadioButton
                            value="CONSUMABLE"
                            label='Consumable'
                        />
                        <RadioButton
                            value="NON_CONSUMABLE"
                            label='Non-Consumable'
                        />
                    </Field>

                    <RaisedButton label="Save" primary={true} onClick={handleSubmit(onSubmit)} />
                </form>
            </div>
        );
    }
}

OrderItemForm = reduxForm({form: 'orderItem'})(OrderItemForm);

export default connect(
    state => ({
        
    }),
    dispatch => ({
        
    })
)(OrderItemForm);