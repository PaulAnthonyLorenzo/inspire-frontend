import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppIcons from '../../constants/AppIcons';
import {FlatButton} from 'material-ui';
import {colors} from 'material-ui/styles';
import {DashboardActions} from '../../actions/DashboardActions';
import StudentTable from '../student/StudentTable';
import EmptyContentPlaceholder from '../common/EmptyContentPlaceholder';
import SearchBar from '../common/SearchBar';

const styles = {
    homeContainer: {
        paddingLeft: 150
    },
    mainContentStyle: {
        padding: 20,
    },
    pageSelector:{
        minWidth: '36px'
    }
};

class Home extends Component {
    componentDidMount() {
        this.props.getAllStudents();          
    }
    
    constructor() {
        super();
        this.state = {
            title: "Home",
            keyword:'',
            currentPage: 0
        };
    }
    
    
    render() {
        return (
            <div>
                <SearchBar 
                    placeholder='Search student by name'
                    onSubmit={this.props.doSearch.bind(this)} 
                    items={this.props.studentHolder.studentList}
                    />
                <div>   
                    {this.props.studentHolder.studentSearchPage.content && this.props.studentHolder.studentSearchPage.content.length > 0 && this.props.studentHolder.studentSearchPage.content != []
                                ? this.renderStudentTable()
                                : this.renderPlaceholder()
                                }
                </div>
                <div style={{display:'flex', justifyContent:'center', fontSize:'24px', paddingTop: '10px'}}>
                    {this.renderPageSelector(this.props.studentHolder.studentSearchPage.totalPages)}
                </div>
            </div>
        );
    }


    renderStudentTable() {
        
        return (
            <StudentTable 
                students={this.props.studentHolder.studentSearchPage.content}
            />
        );
    }

    renderPlaceholder() {
        return (
            <EmptyContentPlaceholder text={
                this.props.keyword
                    ? `No results for "${this.props.keyword}"`
                    : 'Use the search bar to look for a student'
            } />
        );
    }

    handlePageClick(keyword, i) {
        this.setState(prevState => {
        return {
                currentPage: i
            }
        })
        this.props.nextPageSearch(keyword, i)
    }

    renderPageSelector(pages){
        let maxPrevious = 0;
        let maxNext = 0;
        let range = 2;
        if(pages == undefined){
            return <div>Fetching Data</div>
        } else {
            if(this.state.currentPage - 2 > 0){
                maxPrevious = this.state.currentPage - 2;
            } else {
                maxPrevious = 0;
            }
            if(this.state.currentPage + 2 < pages){
                maxNext = this.state.currentPage + 3;
            } else {
                maxNext = pages;
            }
            range = maxNext - maxPrevious;
            return(
                <div>
                {maxPrevious > 0 ? <div style={{display:"inline-block", color:"#E8B23A"}}>...</div>:null}
                {Array(range).fill().map((n, i) => 
                    <FlatButton key={i} style={styles.pageSelector} label={maxPrevious + i + 1} onClick={() => this.handlePageClick(this.state.keyword, (maxPrevious + i))} />
                )}
                {maxNext < pages ? <div style={{display:"inline-block", color:"#E8B23A"}}>...</div>:null}
                </div>
            );
            
        } 
    }
}

export default connect(
	(state, ownProps) => ({
        ...ownProps,
        studentHolder: state.dashboard.students
	}),
	(dispatch, ownProps) => ({
        ...ownProps,

        async doSearch(keyword, page = 0) {
            this.setState({
                keyword: keyword,
                currentPage: page
            })
			await dispatch(DashboardActions.searchStudents(keyword, page))
        },
        
        async nextPageSearch(keyword, page) {
			await dispatch(DashboardActions.searchStudents(keyword, page))
		},
        
        async getAllStudents(){
            await dispatch(DashboardActions.getAllStudents());
        }
	})
)(Home);