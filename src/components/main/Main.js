import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppBar from './AppBar';
import SideBar from './SideBar';
import AppIcons from '../../constants/AppIcons';
import {colors} from 'material-ui/styles'
import {DashboardActions} from '../../actions/DashboardActions';
import {LoginActions} from '../../actions/LoginActions';
import { Route, Redirect } from 'react-router-dom';
import Home from '../home/Home';
import OrderRequest from '../orderRequest/OrderRequest';
import NewOrderRequest from '../orderRequest/NewOrderRequest';
import MyAccount from '../myAccount/MyAccount';
import ChangePass from '../myAccount/ChangePass';
import Routes from '../../constants/Routes';

const styles = {
    homeContainer: {
        // paddingLeft: 150
    },
    mainContentStyle: {
        padding: 20,
    }
};

class Main extends Component {
    
    constructor() {
        super();
        this.state = {
            title: "Home",
            showSideBar: true
        };
    }
    componentWillMount() {    
        this.props.checkLocalStorage();
    }    

    toggleSidebar() {       
        this.setState({showSideBar: !this.state.showSideBar});
    }
    
    render() {        
        const menus = {
            body: [
                {
                    title: 'Dashboard',
                    route: Routes.home,
                    icon: AppIcons.home
                },
                {
                    title: 'Students',
                    route: Routes.login,
                    icon: AppIcons.students
                },
                {
                    title: 'Requests',
                    route: Routes.orderRequest.index,
                    icon: AppIcons.requests,
                    viewBox: '-5 0 50 40'
                },
                {
                    title: 'Non\nConsumables',
                    route: Routes.home,
                    icon: AppIcons.nonConsumables,
                    viewBox: '-12 0 50 40'
                },
                {
                    title: 'Reports',
                    route: Routes.home,
                    icon: AppIcons.reports
                }
            ],
            footer: [
                {
                    title: 'Configurations',
                    route: Routes.home,
                    icon: AppIcons.settings
                },
                {
                    title: 'My Account',
                    route: Routes.myAccount.index,
                    icon: AppIcons.user
                },
                {
                    title: 'Logout',
                    onClick: this.props.logout,
                    icon: AppIcons.logout
                }
            ]
        };

        const {activeUser} = this.props;
        const isLoggedIn = activeUser != null && activeUser != "";
       
        
        return !isLoggedIn  ? <Redirect to="/login" /> : (
            <div id="mainContainer" className={this.state.showSideBar ? "expanded" : ""} style={styles.homeContainer}>
            <SideBar title="Inspire OPS" open={this.state.showSideBar} menuItems={menus} />
            <AppBar title={this.state.title} onLeftIconButtonTouchTap={this.toggleSidebar.bind(this)} />
            <div style={styles.mainContentStyle}>

            <Route path={Routes.home} component={Home} />
            <Route exact path="/" component={Home} />
            <Route exact path={Routes.orderRequest.index} component={OrderRequest} />
            <Route exact path={Routes.orderRequest.new} component={NewOrderRequest} />
            <Route exact path={Routes.myAccount.index} component={MyAccount} />
            <Route exact path={Routes.myAccount.changePassword} component={ChangePass} />
            </div>
            
            </div>
        );
    }
    
    
    renderStudentTable() {
        return (
            <StudentTable 
            students={this.props.students}
            />
        );
    }
    
    renderPlaceholder() {
        return (
            <EmptyContentPlaceholder text={
                this.props.keyword
                ? `No results for "${this.props.keyword}"`
                : 'Use the search bar to look for a student'
            } />
        );
    }
    
}

export default connect(
	(state, ownProps) => ({
        ...ownProps,
        activeUser: state.login.activeUser
	}),
	(dispatch, ownProps) => ({
        ...ownProps,
        logout() {
            dispatch(LoginActions.logout());
        },
        checkLocalStorage() {
            dispatch(LoginActions.checkLocalStorage());
        }
	})
)(Main);