import React, { Component } from 'react';
import MenuItem from 'material-ui/MenuItem';
import Drawer from 'material-ui/Drawer';
import AppSvgIcon from '../common/AppSvgIcon';
import { Link } from 'react-router-dom';
import muiThemeable from 'material-ui/styles/muiThemeable';

const styles =  {
    sideBarTitle: {
        textAlign: "center",
        whiteSpace: "pre-wrap",
        fontSize: 20,
        fontFamily: 'Roboto Condensed, sans-serif',
        flex: 0
    },
    drawerStyle: {
        paddingTop: 20,
        paddingBottom: 20,
        backgroundColor: "#3C3C3C",
        overflow: "hidden",
        display: 'flex',
        flexDirection: 'column'
    },
    drawerContentStyle: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'flex-start',
    },
    inputIconStyle: {
        width: 40,
        height: 40
    },
    menuItemStyle: {
        textAlign: 'center',
        lineHeight: 1,
        fontSize: 14,
        marginBottom: 10,
        paddingTop: 10,
        paddingBottom: 10,
        whiteSpace: "pre-wrap"
    },
    menuItemsContainer: {
        marginTop: "2vh",
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'space-between',
    },
    menuIconStyle: {
        marginBottom: -5
    }
};

class SideBar extends Component {
    
    renderMenuItems(items) {
        
        return (
            <div>
            {
                items.map((item, index) => {
                    
                    const { icon, route, onClick, viewBox } = item;
                    const svg = icon != null && icon != undefined ? <AppSvgIcon viewBox={viewBox}  style={styles.inputIconStyle} icon={icon}/> : '';


                    const itemContent = route != null && route != undefined ? (
                            <Link style={{color: this.props.muiTheme.palette.textColor}} to={route}>
                                <div style={styles.menuIconStyle}>{svg}</div>
                                <div>{item.title}</div>
                            </Link>) : (
                            <div style={{color: this.props.muiTheme.palette.textColor}}>
                                <div style={styles.menuIconStyle}>{svg}</div>
                                <div>{item.title}</div>
                            </div>);
                    
                    return (
                        <MenuItem onClick={onClick} key={index} style={styles.menuItemStyle}>
                            {itemContent}
                        </MenuItem>);
                    })
                }
                </div>
            );
        }
        
    render() {

        const sideBarTitle = this.props.title.split(" ").join("\n").toUpperCase();
        const {body, footer} = this.props.menuItems;            
        const drawerBody = body != null && body != undefined && this.renderMenuItems(body);
        const drawerfooter = footer != null && footer != undefined && this.renderMenuItems(footer);

        return (
            <Drawer containerStyle={styles.drawerStyle} open={this.props.open} width={150} docked={this.props.docked}>
            <div style={styles.drawerContentStyle}>
                <p style={styles.sideBarTitle}>{sideBarTitle}</p>
                <div style={styles.menuItemsContainer}>
                    {drawerBody}
                    {drawerfooter}
                </div>
            </div>
            </Drawer>
        );
    }
    }

    export default muiThemeable()(SideBar);