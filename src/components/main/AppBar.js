import React from 'react';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar';
import PropTypes from 'prop-types'

const OpsAppBar = ( ({title, showMenuIconButton, style, ...props}) => <AppBar title={title} showMenuIconButton={showMenuIconButton} style={style} {...props} /> );

OpsAppBar.propTypes = {
    title: PropTypes.string,
    showMenuIconButton: PropTypes.bool, 
    style: PropTypes.object
}

export default OpsAppBar;