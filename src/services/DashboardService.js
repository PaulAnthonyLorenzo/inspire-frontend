import axios from 'axios';
import ApiConstants from '../constants/ApiConstants';

export class DashboardService {
  
    static async getAll() {
        try {
            let response = await axios({
                method: 'get',
                url: `${ApiConstants.BASE_URL}/students?keyword=&size=7&page=0`,
                withCredentials: true,
                data: {}
            }
        );
            return response.data;
        } catch (error) {
            console.log(error);
        }
    }

    static async getStudentsForAutoSuggest() {
        try {
            let response = await axios({
                method: 'get',
                url: `${ApiConstants.BASE_URL}/students/all`,
                withCredentials: true,
                data: {}
            }
        );
            return response.data;
        } catch (error) {
            console.log(error);
        }
    }

    static async search(keyword, page) {
        try {
            let response = await axios({
                method: 'get',
                url: `${ApiConstants.BASE_URL}/students?keyword=${keyword}&size=7&page=${page}`,
                withCredentials: true,
                data: {}
            }
        );
            return response.data;
        } catch (error) {
            console.log(error);
        }
    }
}