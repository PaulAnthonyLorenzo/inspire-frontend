import axios from 'axios';
import ApiConstants from '../constants/ApiConstants';


export class ForgotPassService {
  
    static async forgotPass(email) {
        try {
            let response = await axios.post(`${ApiConstants.BASE_URL}/resetPass`, email);
            if(response.data != "") {
                return response.data;
            } else {
                return undefined;
            }
            
            
        } catch (error) {
            console.log(error);
        }
    }
}
