import axios from 'axios';

const host = 'http://localhost:8080';

export class MyAccountService {
  
    static async validatePassword(password) {
        const {currentPassword, newPassword} = password;
        try {
            let response = await axios({
              method: 'post',
              url: 'http://localhost:8080/account/change-password',
              data: {currentPassword, newPassword},
              withCredentials: true,
              
            });
            if(response.data == true) {
                return response.data;
            } else {
                return undefined;
            }
            
            return response.data;
        } catch (error) {
            console.log(error);
        }
    }

}
