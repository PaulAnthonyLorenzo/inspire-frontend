import axios from 'axios';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import ApiConstants from '../constants/ApiConstants';


export class LoginService {
  
  static async login(credentials) {
    const {email, password} = credentials;
    try {
      let response = await axios({
        method: 'post',
        url: `${ApiConstants.BASE_URL}/login`,
        auth:
        {username: email, password: password},
        withCredentials: true,
        data: {}
      }
    );
    return response.data;
  } catch (error) {
    console.log(error);
    return new SubmissionError({
      _error: error, // this is general form error
    });
  }
}

static async getLoggedUser() {
  try {
    let response = await axios({
      method: 'get',
      url: `${ApiConstants.BASE_URL}/getLoggedUser`,
      withCredentials: true
    });
    return response.data;
  } catch (error) {
    console.log(error);
  }
}

static async logout() {
  try {
    let response = await axios({
      method: 'post',
      url: `${ApiConstants.BASE_URL}/logout`,
      withCredentials: true,
      data: {}
    });
    return response.data;
  } catch (error) {
    console.log(error);
  }
}
}
