import axios from 'axios';
import ApiConstants from '../constants/ApiConstants';

export class VendorService {
    
      static async getVendorsByType(type) {
          try {
              let response = await axios({
                  method: 'get',
                  url: `${ApiConstants.BASE_URL}/vendor?orderType=SERVICE`,
                //   url: `${ApiConstants.BASE_URL}/vendor?orderType=${type}`,
                  withCredentials: true,
                  data: {}
              }
          );
              return response.data;
          } catch (error) {
              console.log(error);
          }
      }
  
  
  }