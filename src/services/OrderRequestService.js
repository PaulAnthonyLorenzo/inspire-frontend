import axios from 'axios';
import ApiConstants from '../constants/ApiConstants';

export class OrderRequestService {
  
    static async getAllRequestOrders() {
        try {
            let response = await axios({
                method: 'get',
                url: `${ApiConstants.BASE_URL}/request`,
                withCredentials: true,
                data: {}
            }
        );
            return response.data;
        } catch (error) {
            console.log(error);
        }
    }

    static async getFilteredRequest(title) {
        try {
            let response = await axios({
                method: 'get',
                url: `${ApiConstants.BASE_URL}/request/filter?title=${title}`,
                withCredentials: true,
                data: {}
            }
        );
            return response.data;
        } catch (error) {
            console.log(error);
        }
    }

    static async saveOrderRequest(orderRequest) {
        try {
            let response = await axios({
                method: 'post',
                url: `${ApiConstants.BASE_URL}/request`,
                withCredentials: true,
                data: orderRequest
            }
        );
            return response.data;
        } catch (error) {
            console.log(error);
        }
    }

    static async getCount() {
        try {
            let response = await axios({
                method: 'get',
                url: `${ApiConstants.BASE_URL}/request/count`,
                withCredentials: true,
                data: {}
            }
        );
            return response.data;
        } catch (error) {
            console.log(error);
        }
    }


}