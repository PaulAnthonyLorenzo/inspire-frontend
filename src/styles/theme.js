import getMuiTheme from 'material-ui/styles/getMuiTheme';

const theme = getMuiTheme({
    fontFamily: 'Roboto, sans-serif',
    palette: {
        primary1Color: "#04195C",
        accent1Color: "#E8B23A",
        textColor: "#E8B23A",
    },
    appBar: {
        height: 50,
    },
    drawer: {
        backgroundColor: "#3C3C3C"
    }
});

export default theme;

