export default {
    LOGIN_START: 'login',
    LOGIN_SUCCESS: 'login.success',
    LOGIN_FAIL: 'login.fail',
    LOGOUT_SUCCESS: 'logout.success',
    LOGOUT_FAIL: 'logout.fail',
    AUTHORIZE_USER: 'authorize.user',
    LOAD_STUDENTS: 'load.students',
    LOAD_ORDER_REQUEST: 'load.orderRequest',
    LOGIN_CHECK_LOCAL_STORAGE: 'login.checkLocalStorage',
    PASSWORD_RESET: 'password.reset',
    PASSWORD_RESET_SUCCESS: 'password.success',
    PASSWORD_CHANGE: 'password.change',
    PASSWORD_CHANGE_SUCCESS: 'password.change.success',
    PASSWORD_CHANGE_FAILED: 'password.change.failed',

    SHOW_LOADER: 'show.loader',
    HIDE_LOADER: 'hide.loader',
    LOAD_ORDER_REQUESTS: 'load.orderRequests',
    LOAD_VENDORS: 'load.vendors'
}