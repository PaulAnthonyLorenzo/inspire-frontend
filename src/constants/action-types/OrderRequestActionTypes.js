export const OrderRequestActionTypes = {
    LOAD_ORDER_REQUESTS: 'load.orderRequest',
    SAVE_ORDER_REQUEST: 'save.orderRequest',
    SAVE_ORDER_REQUEST_SUCCESS: 'save.orderRequest.success',
    SAVE_ORDER_REQUEST_FAILED: 'save.orderRequest.failed',
    GET_COUNT:'get.count',
    GET_BOX_CLICKED:'get.box.clicked'
}