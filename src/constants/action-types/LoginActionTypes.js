export const LoginActionTypes = {
    LOGIN_START: 'login',
    LOGIN_SUCCESS: 'login.success',
    LOGIN_FAIL: 'login.fail',
    LOGIN_CHECK_LOCAL_STORAGE: 'login.checkLocalStorage',
    LOGOUT_SUCCESS: 'logout.success',
    LOGOUT_FAIL: 'logout.fail',
    AUTHORIZE_USER: 'authorize.user',
    PASSWORD_RESET: 'password.reset',
    PASSWORD_RESET_SUCCESS: 'password.success',
}