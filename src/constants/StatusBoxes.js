import AppIcons from './AppIcons';
import {colors} from 'material-ui/styles';

export default  [
    {
        icon: AppIcons.forApproval,
        title: "For Approval",
        color: colors.blue500,
        id:"FOR_APPROVAL"
    },
    {
        icon: AppIcons.forSuperVisorApproval,
        title: "For Supervisor Approval",
        color: colors.blue500,
        id:"FOR_SUPERVISOR_APPROVAL"
    },
    {
        icon: AppIcons.approve,
        title: "Approved",
        color: colors.blue500  ,
        id:"APPROVED"
    },
    {
        icon: AppIcons.pending,
        title: "For Processing",
        color: colors.blue500,
        id:"FOR_PROCESSING"
    },
    {
        icon: AppIcons.processed,
        title: "Processed",
        color: colors.blue500,
        id:"PROCESSED"
    },
    {
        icon: AppIcons.closed,
        title: "Closed",
        color: colors.blue500,
        id:"CLOSED"
    },
    {
        icon: AppIcons.cancelled,
        title: "Cancelled",
        color: colors.blue500,
        id:"CANCELLED"
    },
    {
        icon: AppIcons.rejected,
        title: "Rejected",
        color: colors.blue500,
        id:"REJECTED"
    },
];
