export default {
    home: '/home',
    student: '/student',
    login: '/login',
    forgotPassword: '/forgot',
    orderRequest : {
        index: '/request',
        new: '/request/new'
    },
    myAccount: {
        index: '/account',
        changePassword: '/account/change-password'
    }
}