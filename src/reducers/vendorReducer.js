import {handleActions} from 'redux-actions';
import {VendorActionTypes} from '../constants/action-types';

const initialState = {
    vendors: []
};

export const vendorReducer = handleActions({

  [VendorActionTypes.LOAD_VENDORS]: (state, action) => {

    
    return {
      ...initialState,
      vendors: action.payload,
    }
  },

  [VendorActionTypes.HIDE_LOADER]: (state, action) => {
    return {
      ...initialState,
      showLoader: false,
      hideLoader: true
    }
  },
  
}, initialState);

