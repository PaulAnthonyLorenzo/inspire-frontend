import {handleActions} from 'redux-actions';
import {LoginActionTypes} from '../constants/action-types';

const initialState = {
  activeUser: null,
  error:{
    errors:{
      _error:{
        response:{
          data:{
            status:null
          }
        }
      }
    }
  }
};

export const loginReducer = handleActions({
  [LoginActionTypes.LOGIN_SUCCESS]: (state, action) => {
    localStorage.setItem('activeUser', action.payload);
    
    return {
      ...initialState,
      isLoggedIn: true,
      activeUser: action.payload
    }
  },
  [LoginActionTypes.LOGIN_FAIL]: (state, action) => {
    return {
      ...initialState,
      activeUser: null,
      error: action.payload
    }
  },
  [LoginActionTypes.AUTHORIZE_USER]: (state, action) => {   
    return {
      ...initialState,
      activeUser: action.payload
    }
  },
  [LoginActionTypes.LOGOUT_SUCCESS]: (state, action) => {
    localStorage.removeItem('activeUser');
    
    return {
      ...initialState,
      activeUser: null
    }
  },
  
}, initialState);

