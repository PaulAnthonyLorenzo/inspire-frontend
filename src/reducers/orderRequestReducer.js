import {handleActions} from 'redux-actions';
import {OrderRequestActionTypes} from '../constants/action-types';

const initialState = {
    orderRequest:[],
    count:[],
    statusFilter:null
};

export const orderRequestReducer = handleActions({
    
	[OrderRequestActionTypes.LOAD_ORDER_REQUESTS]: (state, action) => {       
        return {
            ...state,
            orderRequest: action.payload
        }
    },
    [OrderRequestActionTypes.GET_COUNT]: (state, action) => {       
        return {
            ...state,
            count: action.payload
        }
    },
    [OrderRequestActionTypes.GET_BOX_CLICKED]: (state, action) => {       
        return {
            ...state,
            statusFilter: action.payload
        }
    },
	[OrderRequestActionTypes.SAVE_ORDER_REQUEST]: (state, action) => {      
        return {
            ...initialState,
        }
    },
	[OrderRequestActionTypes.SAVE_ORDER_REQUEST_SUCCESS]: (state, action) => {      
        
        return {
            ...initialState,
        }
    }
    
}, initialState);


