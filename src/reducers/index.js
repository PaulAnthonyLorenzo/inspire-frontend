/* eslint-disable import/newline-after-import */
/* Combine all available reducers to a single root reducer.
 *
 * CAUTION: When using the generators, this file is modified in some places.
 *          This is done via AST traversal - Some of your formatting may be lost
 *          in the process - no functionality should be broken though.
 *          This modifications only run once when the generator is invoked - if
 *          you edit them, they are not updated again.
 */
/* Populated by react-webpack-redux:reducer */
import { combineReducers } from 'redux';
import { reducer as formReducer} from 'redux-form';
import { loginReducer } from './loginReducer';
import {syncHistoryWithStore, routerReducer, routerMiddleware} from 'react-router-redux';
import { dashboardReducer } from './dashboardReducer';
import { orderRequestReducer } from './orderRequestReducer';
import { forgotPassReducer } from './forgotPassReducer';
import { myAccountReducer } from './myAccountReducer';
import { vendorReducer } from './vendorReducer';


const reducers = {
  form: formReducer,
  login: loginReducer,

  vendor: vendorReducer,
  router: routerReducer,
  dashboard: dashboardReducer,
  orderRequest: orderRequestReducer,
  forgotPassReducer: forgotPassReducer,
  myAccountReducer: myAccountReducer
};
const combined = combineReducers(reducers);
module.exports = combined;
