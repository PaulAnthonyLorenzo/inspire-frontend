import {handleActions} from 'redux-actions';
import {UIActionTypes} from '../constants/action-types';

const initialState = {
    showLoader: false,
    hideLoader: false
};

export const uiReducer = handleActions({

  [UIActionTypes.SHOW_LOADER]: (state, action) => {
    return {
      ...initialState,
      showLoader: true,
      hideLoader: false
    }
  },

  [UIActionTypes.HIDE_LOADER]: (state, action) => {
    return {
      ...initialState,
      showLoader: false,
      hideLoader: true
    }
  },
  
}, initialState);

