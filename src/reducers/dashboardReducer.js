import {handleActions} from 'redux-actions';
import {StudentActionTypes} from '../constants/action-types';

const initialState = {
    students:{
        studentSearchPage: {
            content: {}
        },
        studentList:[]
    }
};

export const dashboardReducer = handleActions({
    
	[StudentActionTypes.LOAD_STUDENTS]: (state = {}, action) => ({
        students: action.payload
    })
    
}, initialState);
