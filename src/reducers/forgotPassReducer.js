import {handleActions} from 'redux-actions';
import {LoginActionTypes} from '../constants/action-types';

const initialState = {
  attemptReset: false,
  success: [],
  error: []
};

export const forgotPassReducer = handleActions({
  [LoginActionTypes.PASSWORD_RESET_SUCCESS]: (state, action) => {
    
    return {
      ...initialState,
      attemptReset: true,
      success: [ action.payload ]
    }
  },
  [LoginActionTypes.PASSWORD_RESET_FAIL]: (state, action) => {
    return {
      ...initialState,
      attemptReset: false,
      error: [ action.payload ]
    }
  },

}, initialState);

