import {handleActions} from 'redux-actions';
import ActionTypes from '../constants/ActionTypes';

const initialState = {
  // password: [],
  attemptChange: false,
  success: [],
  error: []
};

export const myAccountReducer = handleActions({
  [ActionTypes.PASSWORD_CHANGE_SUCCESS]: (state, action) => {
    
    return {
      ...initialState,
      attemptChange: true,
      success: [action.payload] 
    }
  },
  [ActionTypes.PASSWORD_CHANGE_FAILED]: (state, action) => {
    return {
      ...initialState,
      attemptChange: false,
      error: [action.payload]
    }
  },

}, initialState);

