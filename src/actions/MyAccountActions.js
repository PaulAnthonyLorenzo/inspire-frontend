import {createAction} from 'redux-actions';
import {MyAccountService} from '../services/MyAccountService';
import ActionTypes from '../constants/ActionTypes';

export class MyAccountActions {
	
	static get changePassSuccess() {
		return createAction(ActionTypes.PASSWORD_CHANGE_SUCCESS);
	}
	
	static get changePassFail() {
		return createAction(ActionTypes.PASSWORD_CHANGE_FAILED);
	}
	
	static validatePassword(password) {
		return async dispatch => {
			try {
				let result = await MyAccountService.validatePassword(password);
				if (result == true ) {
					dispatch(MyAccountActions.changePassSuccess(['Change password success!']));
				} else {
					dispatch(MyAccountActions.changePassFail(['Current password do not match!']));
				}
				dispatch(createAction(ActionTypes.PASSWORD_CHANGE));
			} catch (error) {
				console.log(error);
			}
        }
	}
	
}
