import {createAction} from 'redux-actions';
import {OrderRequestService} from '../services/OrderRequestService';
import {OrderRequestActionTypes} from '../constants/action-types';
import {push} from 'react-router-redux';
import Routes from '../constants/Routes';

export default class OrderRequestActions {

    static get loadOrderRequest() {
        return createAction(OrderRequestActionTypes.LOAD_ORDER_REQUESTS);
    }

    static get getOrderRequestCount(){
        return createAction(OrderRequestActionTypes.GET_COUNT);
    }

    static get saveSuccess() {
        return createAction(OrderRequestActionTypes.SAVE_ORDER_REQUEST_SUCCESS);
    }

    static get saveFailed() {
        return createAction(OrderRequestActionTypes.SAVE_ORDER_REQUEST_FAILED);
    }

    static get getBoxClicked() {
        return createAction(OrderRequestActionTypes.GET_BOX_CLICKED);
    }

    static getAllRequestOrders() {
        return async dispatch => {
            try {
                const orderRequestList = await OrderRequestService.getAllRequestOrders();                
                dispatch(OrderRequestActions.loadOrderRequest(orderRequestList));
            } catch (error) {
                console.log(error);
            }
        }
    }

    static getFilteredRequest(title) {
        return async dispatch => {
            try {
                const orderRequestList = await OrderRequestService.getFilteredRequest(title);                
                dispatch(OrderRequestActions.getBoxClicked(title));
                dispatch(OrderRequestActions.loadOrderRequest(orderRequestList));
            } catch (error) {
                console.log(error);
            }
        }
    }

    static getCount() {
        return async dispatch => {
            try {
                const count = await OrderRequestService.getCount();                
                dispatch(OrderRequestActions.getOrderRequestCount(count));
            } catch (error) {
                console.log(error);
            }
        }
    }

    static saveOrderRequest(orderRequest) {
        return async dispatch => {
            try {
                const response = await OrderRequestService.saveOrderRequest(orderRequest);
                
                if (response) {
                    dispatch(OrderRequestActions.saveSuccess());
                    dispatch(push({
                        pathname: Routes.home,
                        state: {
                          id: 7,
                          color: 'green'
                        }}));
                } else {
                    dispatch(OrderRequestActions.saveFailed());
                }
                dispatch(createAction(OrderRequestActionTypes.SAVE_ORDER_REQUEST));
            } catch (error) {
                console.log(error);
                dispatch(OrderRequestActions.saveFailed());
            }
        }
    }
}