/* eslint-disable import/newline-after-import */
/* Exports all the actions from a single point.

Allows to import actions like so:

import {action1, action2} from '../actions/'
*/
/* Populated by react-webpack-redux:action */

import {DashboardActions} from './DashboardActions';
import {ForgotPassActions} from './ForgotPassActions';
import {LoginActions} from './LoginActions';
import {OrderRequestActions} from './OrderRequestActions';
import {UIActions} from './UIActions';
import {VendorActions} from './VendorActions';

const actions = {
    DashboardActions, 
    ForgotPassActions,
    LoginActions,
    OrderRequestActions,
    UIActions,
    VendorActions};
module.exports = actions;
