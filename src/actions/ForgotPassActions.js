import {createAction} from 'redux-actions';
import {ForgotPassService} from '../services/ForgotPassService';
import {LoginActionTypes} from '../constants/action-types';

export class ForgotPassActions {
	
	static get passwordResetSuccess() {
		return createAction(LoginActionTypes.PASSWORD_RESET_SUCCESS);
	}

	static get passwordResetFail() {
		return createAction(LoginActionTypes.PASSWORD_RESET_FAIL);
	}

	static forgotPass(email) {
		return async dispatch => {
			try {
                let result = await ForgotPassService.forgotPass(email);
				if (result != undefined) {
					dispatch(ForgotPassActions.passwordResetSuccess(["Password reset success! Please check your email for further instructions"]));
				} else {
					dispatch(ForgotPassActions.passwordResetFail(["Email not found!"]));
				}
				dispatch(createAction(LoginActionTypes.PASSWORD_RESET));
			} catch (error) {
				console.log(error);
			}
		}
		return createAction(LoginActionTypes.PASSWORD_RESET);
	}
	
}
