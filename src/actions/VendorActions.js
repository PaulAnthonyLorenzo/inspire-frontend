import {createAction} from 'redux-actions';
import {VendorService} from '../services/VendorService';
import {VendorActionTypes} from '../constants/action-types';

export class VendorActions {
    static get loadVendors() {
        return createAction(VendorActionTypes.LOAD_VENDORS);
    }
    
    static getVendorsByType(type) {
        return async dispatch => {
            // try {
                const vendors = await VendorService.getVendorsByType(type);                
                dispatch(VendorActions.loadVendors(vendors));
            // } catch (error) {
            //     console.log(error);
            // }
        }
    }
}