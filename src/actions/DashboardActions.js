import {createAction} from 'redux-actions';
import {DashboardService} from '../services/DashboardService';
import {StudentActionTypes} from '../constants/action-types';

export class DashboardActions {

    static get loadStudents() {
        return createAction(StudentActionTypes.LOAD_STUDENTS);
    }

    static getAllStudents() {
        return async dispatch => {
            try {
                const students = await DashboardService.getAll();
                dispatch(DashboardActions.loadStudents(students));
            } catch (error) {
                console.log(error);
            }
        }
    }

    static searchStudents(keyword, page) {
        return async dispatch => {
            try {
                const students = await DashboardService.search(keyword, page);
                dispatch(DashboardActions.loadStudents(students));
            } catch (error) {
                console.log(error);
            }
        }
    }
}