import {createAction} from 'redux-actions';
import {UIActionTypes} from '../constants/action-types';

export class UIActions {
	
	static get showLoader() {
		return createAction(UIActionTypes.SHOW_LOADER);
    }
    
    static get hideLoader() {
		return createAction(UIActionTypes.HIDE_LOADER);
	}
	
}
