import {createAction} from 'redux-actions';
import {LoginService} from '../services/LoginService';
import {LoginActionTypes} from '../constants/action-types';

export class LoginActions {
	
	static get loginStart() {
		return createAction(LoginActionTypes.LOGIN_START);
	}
	
	static get loginSuccess() {
		return createAction(LoginActionTypes.LOGIN_SUCCESS);
	}
	
	static get loginFail() {
		return createAction(LoginActionTypes.LOGIN_FAIL);
	}
	
	static get logoutSuccess() {
		return createAction(LoginActionTypes.LOGOUT_SUCCESS);
	}
	
	static get logoutFail() {
		return createAction(LoginActionTypes.LOGOUT_FAIL);
	}
	
	static checkLocalStorage() {
		return async dispatch => {
			try {
				const activeUser = localStorage.getItem('activeUser');
				
				if (activeUser != null && activeUser != "") {
					dispatch(LoginActions.loginSuccess(activeUser));
				} else {
					dispatch(LoginActions.loginFail());
				}
				dispatch(createAction(LoginActionTypes.LOGIN_CHECK_LOCAL_STORAGE));
			} catch (error) {
				console.log(error);
			}
		}
	}
	
	static login(credentials) {
		return async dispatch => {
			try {
				let result = await LoginService.login(credentials);
				if (result != undefined && result === "Successful Login") {
					
					dispatch(LoginActions.loginSuccess(credentials.email));
				} else {
					dispatch(LoginActions.loginFail(result));
				}
				dispatch(LoginActions.loginStart());
			} catch (error) {
				console.log(error);
			}
		}
	}
	
	static logout() {
		return async dispatch => {
			try {
				let result = await LoginService.logout();
				
				if (result != undefined) {
					dispatch(LoginActions.logoutSuccess());
				} else {
					dispatch(LoginActions.logoutFail());
				}
			} catch (error) {
				console.log(error);
			}
		}
	}
}
